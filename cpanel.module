<?php
/**
 * @file
 * Syncronize users with cPanel email and FTP accounts.
 */

/**
 * Implements hook_menu().
 */
function cpanel_menu() {
  $items = array();
	$items['admin/config/cpanel'] = array(
    'title' => 'cPanel',
    'description' => 'cPanel tools.',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer cpanel'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );
  $items['admin/config/cpanel/settings'] = array(
    'title' => 'cPanel Settings',
    'description' => 'Setup cPanel credentials.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cpanel_admin'),
    'access arguments' => array('administer cpanel'),
  );
  $items['admin/config/cpanel/email'] = array(
    'title' => 'cPanel Manage Email',
    'description' => 'Manage cPanel email accounts',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cpanel_email'),
    'access arguments' => array('administer cpanel'),
  );
  $items['admin/config/cpanel/ftp'] = array(
    'title' => 'cPanel Manage FTP',
    'description' => 'Manage cPanel ftp accounts',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cpanel_ftp'),
    'access arguments' => array('Administer cPanel'),
  );
  $items['manage/cpanelaccount'] = array(
    'title' => 'cPanel Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cpanel_create_account'),
    'access arguments' => array('access cpanel'),
    'menu_name' => 'user-menu',
  );
  return $items;
}
/**
 * Implements hook_menu().
 */
function cpanel_permission() {
  return array(
    'access cpanel' => array(
      'title' => t('Access cPanel'),
      'description' => t('Can have cPanel Accounts.'),
    ),
    'administer cpanel' => array(
      'title' => t('Administer cPanel'),
      'description' => t('Perform administration setting of cPanel module.'),
    ),
  );
}
/**
 * Callback function for admin setting.
 */
function cpanel_admin() {
  $form['server'] = array(
    '#type' => 'fieldset',
    '#title' => t('cPanel server settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['server']['cpanel_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => variable_get('cpanel_server', 'cpanel.example.com'),
    '#size' => 25,
    '#maxlength' => 80,
    '#description' => t('Name of the cPanel administrative server.'),
  );
  $form['server']['cpanel_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#default_value' => variable_get('cpanel_port', '2082'),
    '#size' => 4,
    '#maxlength' => 4,
    '#description' => t('cPanel server port.'),
  );
  $form['server']['cpanel_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('cpanel_user', ''),
    '#size' => 25,
    '#maxlength' => 64,
    '#description' => t('Username for cpanel administrative account.'),
  );
  $form['server']['cpanel_pass'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#default_value' => variable_get('cpanel_pass', ''),
    '#size' => 25,
    '#maxlength' => 64,
    '#description' => t('User password for cpanel administrative account.'),
    '#required' => TRUE,
  );
  $form['email'] = array(
    '#type' => 'fieldset',
    '#title' => t('eMail accounts settings'),
    '#description' => t('This module automatically creates email accounts in the form <strong>username@%domain</strong> for each Drupal user.', array('%domain' => variable_get('cpanel_mail_domain', 'example.com'))),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['email']['cpanel_mail_enabled'] = array(
    '#type' => 'radios',
    '#title' => t('Email Accounts Creation Status'),
    '#default_value' => variable_get('cpanel_mail_enabled', 0),
    '#options' => array(t('Disabled'), t('Enabled')),
  );
  $form['email']['cpanel_mail_quota'] = array(
    '#type' => 'textfield',
    '#title' => t('Mail quota'),
    '#default_value' => variable_get('cpanel_mail_quota', 2),
    '#size' => 3,
    '#maxlength' => 2,
    '#description' => t('Amount of disk space the account can use (Mb).'),
  );
  $form['email']['cpanel_mail_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Mail domain'),
    '#default_value' => variable_get('cpanel_mail_domain', 'example.com'),
    '#size' => 40,
    '#maxlength' => 100,
    '#description' => t('Domain or subdomain under email accounts will be created. This domain <a href="http://%cpanel_server:%cpanel_port/frontend/x/subdomain/index.html">must be created previously</a>.',
      array(
        '%cpanel_server' => variable_get('cpanel_server', 'cpanel.example.com'),
        '%cpanel_port' => variable_get('cpanel_port', '2082'),
      )
    ),
  );
  $form['ftp'] = array(
    '#type' => 'fieldset',
    '#title' => t('FTP accounts settings'),
    '#description' => t('You users may need to transfer files from a computer to the computer that contains your web site. If they need to transfer may files, using a <a href="http://en.wikipedia.org/wiki/File_Transfer_Protocol">FTP</a> client is the quickest way to accomplish this. This module automatically creates <a href="http://en.wikipedia.org/wiki/File_Transfer_Protocol">FTP</a> accounts in the form <strong>username@%domain</strong> which allow users to access the folder <strong>/home/%cpanel_user/public_html%cpanel_ftp_subrootusername</strong> with a <a href="http://en.wikipedia.org/wiki/File_Transfer_Protocol">FTP</a> client.',
      array(
        '%domain' => variable_get('cpanel_ftp_domain', 'example.com'),
        '%cpanel_user' => variable_get('cpanel_user', 'user'),
        '%cpanel_ftp_subroot' => variable_get('cpanel_ftp_subroot', '/'),
      )
    ),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['ftp']['cpanel_ftp_enabled'] = array(
    '#type' => 'radios',
    '#title' => t('FTP Accounts Creation Status'),
    '#default_value' => variable_get('cpanel_ftp_enabled', 0),
    '#options' => array(t('Disabled'), t('Enabled')),
  );
  $form['ftp']['cpanel_ftp_quota'] = array(
    '#type' => 'textfield',
    '#title' => t('FTP quota'),
    '#default_value' => variable_get('cpanel_ftp_quota', 10),
    '#size' => 3,
    '#maxlength' => 2,
    '#description' => t('Amount of disk space the account can use (Mb).'),
  );
  $form['ftp']['cpanel_ftp_port'] = array(
    '#type' => 'textfield',
    '#title' => t('FTP Port'),
    '#default_value' => variable_get('cpanel_ftp_port', '21'),
    '#size' => 40,
    '#maxlength' => 100,
    '#description' => t('FTP Port connect to server.'),
  );
  $form['ftp']['cpanel_ftp_subroot'] = array(
    '#type' => 'textfield',
    '#title' => t('FTP directory root'),
    '#default_value' => variable_get('cpanel_ftp_subroot', '/'),
    '#size' => 40,
    '#maxlength' => 100,
    '#description' => t('Directory which the account will have access under <i>/home/%cpanel_user/public_html</i> folder. This folder <a href="http://%cpanel_server:%cpanel_port/frontend/x/files/index.html">must be created previously</a>.',
      array(
        '%cpanel_user' => variable_get('cpanel_user', 'user'),
        '%cpanel_server' => variable_get('cpanel_server', 'cpanel.example.com'),
        '%cpanel_port' => variable_get('cpanel_port', '2082'),
      )
    ),
  );
  $form['ftp']['cpanel_ftp_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('FTP global domain'),
    '#default_value' => variable_get('cpanel_ftp_domain', 'example.com'),
    '#size' => 40,
    '#maxlength' => 100,
    '#description' => t('Global domain of your website.'),
  );
  return system_settings_form($form);
}
/**
 * Callback function for email setting.
 */
function cpanel_email() {
  $form['email_quota'] = array(
    '#type' => 'fieldset',
    '#title' => t('Massive quota change'),
    '#description' => t('A mail quota is the amount of space reserved on the server to house email messages.'),
    '#weight' => 0,
  );
  $form['email_quota']['cpanel_mail_quota'] = array(
    '#type' => 'textfield',
    '#title' => t('Mail quota'),
    '#default_value' => variable_get('cpanel_mail_quota', 2),
    '#size' => 3,
    '#maxlength' => 2,
    '#description' => t('Mailbox quota for each user account (Mb).'),
  );
  $form['email_quota']['op'] = array(
    '#type' => 'submit',
    '#value' => t('Change Email Quota'),
  );
  $form['email_aging'] = array(
    '#type' => 'fieldset',
    '#title' => t('Massive aging change'),
    '#description' => t('Mail aging is the process in which e-mails will be removed from the server automatically, upon successful log out of a pop3 session. This deletion will only occur if the messages are older then a specified amount of days.'),
    '#weight' => 1,
  );
  $form['email_aging']['cpanel_aging_days'] = array(
    '#type' => 'textfield',
    '#title' => t('Days'),
    '#default_value' => variable_get('cpanel_aging_days', 8),
    '#size' => 3,
    '#maxlength' => 2,
    '#description' => t('Number of days to keep e-mail.'),
  );
  $form['email_aging']['op'] = array(
    '#type' => 'submit',
    '#value' => t('Change Email Aging'),
  );
  $form['email_delete'] = array(
    '#type' => 'fieldset',
    '#title' => t('Massive accounts deletion'),
    '#weight' => 2,
  );
  $form['email_delete']['op'] = array(
    '#type' => 'submit',
    '#value' => t('Delete Email Accounts'),
  );
  return $form;
}
/**
 * Sumit Function for email manage form.
 */
function cpanel_email_submit(&$form, $form_state) {
  if ($form_state['values']['op'] == 'Delete Email Accounts') {
    $batch = array(
      'operations' => array(
        array('cpanel_email_accounts_batch_process', array('delete_email', '')),
      ),
      'finished' => 'cpanel_accounts_batch_finished',
      'title' => t('Processing Email Delete Batch'),
      'init_message' => t('Email Delete Batch is starting.'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('Email Delete Batch has encountered an error.'),
    );
    batch_set($batch);
    batch_process('admin/config/cpanel/email');
  }
  elseif ($form_state['values']['op'] == 'Change Email Quota') {
    variable_set('cpanel_mail_quota', $form_state['values']['cpanel_mail_quota']);
    $batch = array(
      'operations' => array(
        array('cpanel_email_accounts_batch_process',
          array('update_email_quota', $form_state['values']['cpanel_mail_quota']),
        ),
      ),
      'finished' => 'cpanel_accounts_batch_finished',
      'title' => t('Processing Email Update Quota Batch'),
      'init_message' => t('Email Update Quota Batch is starting.'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('Email Update Quota Batch has encountered an error.'),
    );
    batch_set($batch);
    batch_process('admin/config/cpanel/email');
  }
  elseif ($form_state['values']['op'] == 'Change Email Aging') {
    variable_set('cpanel_aging_days', $form_state['values']['cpanel_aging_days']);
    $batch = array(
      'operations' => array(
        array('cpanel_email_accounts_batch_process',
          array('update_email_aging', $form_state['values']['cpanel_aging_days']),
        ),
      ),
      'finished' => 'cpanel_accounts_batch_finished',
      'title' => t('Processing Email Change Aging Batch'),
      'init_message' => t('Email Change Aging Batch is starting.'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('Email Change Aging Batch has encountered an error.'),
    );
    batch_set($batch);
    batch_process('admin/config/cpanel/email');
  }
}
/**
 * Callback function for ftp setting.
 */
function cpanel_ftp() {
  $form['ftp_quota'] = array(
    '#type' => 'fieldset',
    '#title' => t('Massive quota change'),
    '#description' => t('A FTP quota is the amount of space reserved on the server to house files.'),
    '#weight' => 0,
  );
  $form['ftp_quota']['cpanel_ftp_quota'] = array(
    '#type' => 'textfield',
    '#title' => t('FTP quota'),
    '#default_value' => variable_get('cpanel_ftp_quota', 2),
    '#size' => 3,
    '#maxlength' => 2,
    '#description' => t('FTP quota for each user account (Mb).'),
  );
  $form['ftp_quota']['op'] = array(
    '#type' => 'submit',
    '#value' => t('Change FTP Quota'),
    '#button_type' => 'submit',
  );
  $form['ftp_delete'] = array(
    '#type' => 'fieldset',
    '#title' => t('Massive accounts deletion'),
    '#weight' => 1,
  );
  $form['ftp_delete']['op'] = array(
    '#type' => 'submit',
    '#value' => t('Delete FTP Accounts'),
  );
  return $form;
}
/**
 * Submit function for ftp setting.
 */
function cpanel_ftp_submit(&$form, $form_state) {
  if ($form_state['values']['op'] == 'Delete FTP Accounts') {
    $batch = array(
      'operations' => array(
        array('cpanel_ftp_accounts_batch_process', array('delete', '')),
      ),
      'finished' => 'cpanel_accounts_batch_finished',
      'title' => t('Processing FTP Delete Batch'),
      'init_message' => t('FTP Delete Batch is starting.'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('FTP Delete Batch has encountered an error.'),
    );
    batch_set($batch);
    batch_process('admin/config/cpanel/ftp');
  }
  elseif ($form_state['values']['op'] == 'Change FTP Quota') {
    varialbe_set('cpanel_ftp_quota', $form_state['values']['cpanel_ftp_quota']);
    $batch = array(
      'operations' => array(
        array('cpanel_ftp_accounts_batch_process',
          array('update_quota', $form_state['values']['cpanel_ftp_quota']),
        ),
      ),
      'finished' => 'cpanel_accounts_batch_finished',
      'title' => t('Processing FTP Update Quota Batch'),
      'init_message' => t('FTP Update Quota Batch is starting.'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('FTP Update Quota Batch has encountered an error.'),
    );
    batch_set($batch);
    batch_process('admin/config/cpanel/ftp');
  }
}
/**
 * FTP Account batch process function.
 */
function cpanel_ftp_accounts_batch_process($option, $quota, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = db_query("SELECT COUNT(name) as item_count FROM {variable} WHERE name LIKE '%ftp_credentials_%'")->fetchField();
  }
  // With each pass through the callback, retrieve the next group of nids.
  $result = db_query("SELECT * FROM `variable` WHERE `name` LIKE '%ftp_credentials_%'");
  foreach ($result as $row) {
    $ftp_credentials = unserialize($row->value);
    $username = $ftp_credentials['items'][2];
    $username1 = explode(':', $username);
    $username = trim($username1[1]);
    if ($option == 'delete') {
      // Here we actually perform our process.
      $ret = _cpanel_handle_request('/frontend/x3/ftp/realdodelftp.html', array('login' => $username), variable_get('cpanel_ftp_domain', ''));
      if (!$ret) {
        watchdog('user', 'Error deleting cPanel FTP account <em>' . $username . '</em>', WATCHDOG_ERROR);
      }
      else {
        watchdog('user', 'Deleted cPanel FTP account <em>' . $username . '</em>', WATCHDOG_NOTICE);
        variable_del($row->name);
      }
    }
    elseif ($option == 'update_quota') {
      $name = str_replace(strrchr($username, '@'), '', $username);
      // Here we actually perform our processing on the current node.
      $ret = _cpanel_handle_request('/frontend/x3/ftp/doeditquota.html', array('acct' => $name, 'quota' => $quota), variable_get('cpanel_ftp_domain', ''));
      if (!$ret) {
        watchdog('user', 'Error changing quota for cPanel FTP account <em>' . $username . '</em>', WATCHDOG_ERROR);
      }
      else {
        watchdog('user', 'Quota changed for cPanel FTP account <em>' . $username . '</em>', WATCHDOG_ERROR);
      }
    }
    // Store some result for post-processing in the finished callback.
    $context['results'][] = 'FTP account ' . $username . ' deleted.';
    // Update our progress information.
    $context['sandbox']['progress']++;
    // Setup message.
    $context['message'] = t('Now processing %login', array('%login' => $username));
  }
  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}
/**
 * Batch 'finished' callback.
 */
function cpanel_accounts_batch_finished($success, $results, $operations) {
  if ($success) {
    // Here we do something meaningful with the results.
    $message = count($results) . ' items processed.';
    $message .= theme('item_list', $results);
    drupal_set_message($message);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments',
      array(
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE),
      )
    );
    drupal_set_message($message, 'error');
  }
}
/**
 * Callback function for create ftp account.
 */
function cpanel_create_account() {
  global $user;
  $cpanel_mail_domain = variable_get('cpanel_mail_domain', '');
  $cpanel_ftp_domain = variable_get('cpanel_ftp_domain', '');
  $cpanel_username = cpanel_formatname($user->name);
  $form['cpanel'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email & FTP accounts'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 0,
  );
  if ($cpanel_mail_domain != '' && variable_get('cpanel_mail_enabled', 0) && user_access('access cpanel', $user)) {
    $form['cpanel']['cpanel_email_' . $user->uid] = array(
      '#type' => 'checkbox',
      '#title' => t('Create an email account with my username'),
      '#return_value' => 1,
      '#default_value' => variable_get('cpanel_email_' . $user->uid, 0),
      '#description' => t('Check to create an email account with your username. You can connect to <i>%server</i> with your <a href="http://en.wikipedia.org/wiki/Internet_Message_Access_Protocol" target="_blank">IMAP</a>/<a href="http://en.wikipedia.org/wiki/Post_Office_Protocol" target="_blank">POP3</a> client.', array('%server' => 'mail.' . $cpanel_ftp_domain)),
    );
    $form['cpanel']['cpanel_email_info'] = array(
      '#value' => '<div align="center"><strong>' . $cpanel_username . '@' . $cpanel_mail_domain . '</strong></div>',
    );
  }
  if ($cpanel_ftp_domain != '' && variable_get('cpanel_ftp_enabled', 0) && user_access('access cpanel', $user)) {
    $form['cpanel']['cpanel_ftp_' . $user->uid] = array(
      '#type' => 'checkbox',
      '#title' => t('Create a FTP account with my username'),
      '#return_value' => 1,
      '#default_value' => variable_get('cpanel_ftp_' . $user->uid),
      '#description' => t('Check to create a FTP account with your username. You can connect to <i>%server</i> with your <a href="http://en.wikipedia.org/wiki/File_Transfer_Protocol" target="_blank">FTP</a> client.', array('%server' => 'ftp.' . $cpanel_ftp_domain)),
    );
    $form['cpanel']['cpanel_ftp_info'] = array(
      '#value' => '<div align="center"><strong>' . $cpanel_username . '@' . $cpanel_ftp_domain . '</strong> (<a href="' . variable_get('cpanel_ftp_subroot', '/') . $cpanel_username . '" target="blank">' . variable_get('cpanel_ftp_subroot', '/') . $cpanel_username . '</a>) </div>',
    );
  }
  if (variable_get('ftp_credentials_' . $user->uid, '') != '') {
    $form['cpanel_ftp'] = array(
      '#type' => 'fieldset',
      '#title' => t('Your FTP Details'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#weight' => 1,
    );
    $form['cpanel_ftp']['cpanel_ftp_details'] = array(
      '#type' => 'markup',
      '#markup' => theme('item_list', variable_get('ftp_credentials_' . $user->uid, '')),
    );
  }
  if (variable_get('email_credentials_' . $user->uid) != '') {
    $form['cpanel_email'] = array(
      '#type' => 'fieldset',
      '#title' => t('Your Email Details'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#weight' => 2,
    );
    $form['cpanel_email']['cpanel_email_details'] = array(
      '#type' => 'markup',
      '#markup' => theme('item_list', variable_get('email_credentials_' . $user->uid, '')),
    );
  }
  $form['cpanel']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}
/**
 * Submit function for cPanel Create Account form.
 */
function cpanel_create_account_submit(&$form, $form_state) {
  global $user;
  $username = preg_replace("/[^a-zA-z0-9_\-]/", "", strtolower($user->name));
  variable_set('cpanel_email_' . $user->uid, $form_state['values']['cpanel_email_' . $user->uid]);
  variable_set('cpanel_ftp_' . $user->uid, $form_state['values']['cpanel_ftp_' . $user->uid]);
  if (variable_get('cpanel_mail_domain', '') != '' && variable_get('cpanel_mail_enabled', 0)) {
    if ($form_state['values']['cpanel_email_' . $user->uid]) {
      if (variable_get('email_credentials_' . $user->uid, '') != '') {
        // Update password of existing account.
      }
      else {
        // Create new accoount.
        $email_pass = user_password(6);
        $ret = _cpanel_handle_request('/frontend/x3/mail/doaddpop.html',
          array(
            'email' => $username,
            'password' => $email_pass, 'domain' => variable_get('cpanel_mail_domain', ''),
            'quota' => variable_get('cpanel_mail_quota', 2)), variable_get('cpanel_mail_domain')
          );
        if (!$ret) {
          watchdog('user', 'Error creating cPanel email account <em>' . $username . '@' . variable_get('cpanel_mail_domain') . '</em>', WATCHDOG_ERROR);
        }
        else {
          drupal_set_message('Your Email account <em>' . $username .  '@' . variable_get('cpanel_mail_domain') . '</em> created.');
          $email_credentials['items'] = array(
            'email : ' . $username . '@' . variable_get('cpanel_mail_domain'),
            'password : ' . $email_pass,
          );
          variable_set('email_credentials_' . $user->uid, $email_credentials);
          watchdog('user', 'Created cPanel email account <em>' . $username . '@' . variable_get('cpanel_mail_domain') . '</em> with password : ' . $email_pass . ' at ' . date("H:i:s"), WATCHDOG_NOTICE);
        }
      }
    }
    else {
      // Delete Account.
      if (variable_get('email_credentials_' . $user->uid, '') != '') {
        $ret = _cpanel_handle_request('/frontend/x3/mail/realdelpop.html', array(
          'email' => $username,
          'domain' => variable_get('cpanel_mail_domain', ''),
        ), variable_get('cpanel_mail_domain', ''));
        if (!$ret) {
          watchdog('user', 'Error deleting cPanel Email account <em>' . $username . '@' . variable_get('cpanel_mail_domain') . '</em>', WATCHDOG_ERROR);
        }
        else {
          drupal_set_message('Your Email account <em>' . $username . '@' . variable_get('cpanel_mail_domain') . '</em> deleted.');
          watchdog('user', 'Deleted cPanel Email account <em>' . $username . '@' . variable_get('cpanel_mail_domain') . '</em>', WATCHDOG_NOTICE);
          variable_del('email_credentials_' . $user->uid);
        }
      }
    }
  }
  if (variable_get('cpanel_ftp_domain', '') != '' && variable_get('cpanel_ftp_enabled', 0)) {
    if ($form_state['values']['cpanel_ftp_' . $user->uid]) {
      if (variable_set('ftp_credentials_' . $user->uid, '') != '') {
        // Update password.
      }
      else {
        // Create new account.
        $ftp_pass = user_password(6);
        $ret = _cpanel_handle_request('/cpsess6421711980/frontend/x3/ftp/doaddftp.html',
          array(
            'password' => $ftp_pass,
            'login' => $username,
            'homedir' => variable_get('cpanel_ftp_subroot', '/') . $username,
            'quota' => variable_get('cpanel_ftp_quota', 2)), variable_get('cpanel_ftp_domain')
          );
        if (!$ret) {
          watchdog('user', 'Error creating cPanel FTP account <em>' . $username . '@' . variable_get('cpanel_server', '') . '</em>', WATCHDOG_ERROR);
        }
        else {
          drupal_set_message('Your FTP account <em>' . $username . '@' . variable_get('cpanel_server', '') . '</em> created.');
          $ftp_credentials['items'] = array(
            'host : ' . variable_get('cpanel_ftp_domain', ''),
            'port : ' . variable_get('cpanel_ftp_port', '21'),
            'username : ' . $username . '@' . variable_get('cpanel_server', ''),
            'password : ' . $ftp_pass,
          );
          variable_set('ftp_credentials_' . $user->uid, $ftp_credentials);
          watchdog('user', 'Created cPanel FTP account <em>' . $username . '@' . variable_get('cpanel_server', '') . '</em> with password :' . $ftp_pass . ' at ' . date("H:i:s"), WATCHDOG_NOTICE);
        }
      }
    }
    else {
      // Delete account.
      if (variable_get('ftp_credentials_' . $user->uid, '') != '') {
        $ret = _cpanel_handle_request('/frontend/x3/ftp/realdodelftp.html', array('login' => $username), variable_get('cpanel_ftp_domain', ''));
        if (!$ret) {
          watchdog('user', 'Error deleting cPanel FTP account <em>' . $username . '@' . variable_get('cpanel_server', '') . '</em>', WATCHDOG_ERROR);
        }
        else {
          drupal_set_message('Your FTP account <em>' . $username . '@' . variable_get('cpanel_server', '') . '</em> deleted.');
          watchdog('user', 'Deleted cPanel FTP account <em>' . $username . '@' . variable_get('cpanel_server', '') . '</em>', WATCHDOG_NOTICE);
          variable_del('ftp_credentials_' . $user->uid);
        }
      }
    }
  }
}
/**
 * FTP Account batch process function.
 */
function cpanel_email_accounts_batch_process($option1, $option2, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = db_query("SELECT COUNT(name) as item_count FROM {variable} WHERE name LIKE '%email_credentials_%'")->fetchField();
  }
  // With each pass through the callback, retrieve the next group of nids.
  $result = db_query("SELECT * FROM `variable` WHERE `name` LIKE '%email_credentials_%'");
  foreach ($result as $row) {
    $email_credentials = unserialize($row->value);
    $username = $email_credentials['items'][0];
    $username1 = explode(':', $username);
    $username = trim($username1[1]);
    $name = explode('@', $username);
    if ($option1 == 'delete_email') {
      // Here we actually perform our process.
      $ret = _cpanel_handle_request('/frontend/x3/mail/realdelpop.html', array(
        'email' => $name[0],
        'domain' => variable_get('cpanel_mail_domain', ''),
      ), variable_get('cpanel_mail_domain', ''));
      if (!$ret) {
        watchdog('user', 'Error deleting cPanel Email account <em>' . $username . '</em>', WATCHDOG_ERROR);
      }
      else {
        watchdog('user', 'Deleted cPanel Email account <em>' . $username . '</em>', WATCHDOG_NOTICE);
        variable_del($row->name);
      }
    }
    elseif ($option1 == 'update_email_quota') {
      // Here we actually perform our processing on the current node.
      $ret = _cpanel_handle_request('/frontend/x3/mail/doeditquota.html', array(
        'email' => $name[0],
        'domain' => variable_get('cpanel_mail_domain', ''),
        'quota' => $option2,
      ), variable_get('cpanel_ftp_domain', ''));
      if (!$ret) {
        watchdog('user', 'Error changing quota for cPanel Email account <em>' . $username . '</em>', WATCHDOG_ERROR);
      }
      else {
        watchdog('user', 'Quota changed for cPanel Email account <em>' . $username . '</em>', WATCHDOG_ERROR);
      }
    }
    elseif ($option1 == 'update_email_aging') {
      // Here we actually perform our processing on the current node.
      $ret = _cpanel_handle_request('/frontend/x/mail/doaging.html', array('user' => $username, 'numdays' => $option2), variable_get('cpanel_mail_domain', ''));
      if (!$ret) {
        watchdog('user', 'Error changing aging for cPanel Email account <em>' . $username . '</em>', WATCHDOG_ERROR);
      }
      else {
        watchdog('user', 'Aging changed for cPanel Email account <em>' . $username . '</em>', WATCHDOG_ERROR);
      }
    }
    // Store some result for post-processing in the finished callback.
    $context['results'][] = 'Email account ' . $username . ' deleted.';
    // Update our progress information.
    $context['sandbox']['progress']++;
    // Setup message.
    $context['message'] = t('Now processing %login', array('%login' => $username));
  }
  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}
/**
 * Other functions.
 */
function cpanel_formatname($username) {
  return preg_replace("/[^a-zA-z0-9_\-]/", "", strtolower($username));
}
/**
 * Functional to handle rquest to cPanel.
 */
function _cpanel_handle_request($path, $params, $domain) {
  $req = array();
  foreach ($params as $key => $value) {
    $req[] = $key . '=' . $value;
  }
  $request = implode('&', $req);
  // Generate POST headers.
  $headers = array();
  if (variable_get('cpanel_user', '') != '') {
    $headers['Authorization'] = 'Basic ' . base64_encode(variable_get('cpanel_user', '') . ':' . variable_get('cpanel_pass', ''));
  }
  $headers['Content-Type'] = 'application/x-www-form-urlencoded';
  // Request.
  $cpanel_user = variable_get('cpanel_user', '');
  $cpanel_pass = variable_get('cpanel_pass', '');
  $cpanel_port = variable_get('cpanel_port', '2082');
  $url = url('https://' . $cpanel_user . ':' . $cpanel_pass . '@' . $domain . ':' . $cpanel_port . $path . '?' . $request);
  $result = drupal_http_request($url, $headers, 'POST');
  // Process HTTP response code.
  switch ($result->code) {
    case 304:
      return FALSE;

    break;
    case 301:
      return FALSE;

    break;
    case 200:
    case 302:
    case 307:
      return TRUE;

    break;
    default:
      return TRUE;
  }
}
